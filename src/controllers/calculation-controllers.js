import { calculateAmountPayable } from '../services';

export const amountToPay = async (req, res) => {
  const { cnpj, data_inicio, data_fim } = req.body;

  const calculatedValue = 
    await calculateAmountPayable(cnpj, data_inicio, data_fim);
  
  const response = { valor_calculado: calculatedValue };
  return res.status(200).json(response);
};

export default { amountToPay };
