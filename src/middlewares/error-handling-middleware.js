export const errorHandlingMiddleware = (error, req, res, next) => {
  const statusCode = error.statusCode ?? 500;
  const message = error.statusCode ? error.message : error.message;

  return res.status(statusCode).json({ message });
};
