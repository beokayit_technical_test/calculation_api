import http from '../http/axios';
import { NotFound, BadRequest } from '../helpers/errors';

export const calculateAmountPayable = async (cnpj, start_date, end_date) => {
  const hourValue = await findCompanyAndGetHourValue(cnpj);
  const numberOfHolidays = await findAndCountNumberOfHolidays(start_date, end_date);
  const numberOfWorkingDaysOfTheWeek = countNumberOfWorkingDaysOfTheWeek(start_date, end_date);

  const numberOfDaysWorked = numberOfWorkingDaysOfTheWeek - numberOfHolidays;
  const totalHoursWorked = numberOfDaysWorked * 8;
  const totalPayable = totalHoursWorked * hourValue;
  const calculatedValue = parseFloat(totalPayable).toFixed(2);

  return calculatedValue;
};

const findCompanyAndGetHourValue = async (cnpj) => {
  const response = await http.get(`empresas?cnpj=${cnpj}`);
  const company = response.data;

  if (!company.length)
    throw new NotFound('Empresa como o CNPJ informado não encontrada');

  const hourValue = parseFloat(company[0].hour_value);
  return hourValue;
};

const findAndCountNumberOfHolidays = async (start_date, end_date) => {
  const response = await http.get(
    `feriados?data_inicio=${start_date}&data_fim=${end_date}`,
  );
  const holiday = response.data;

  if (holiday.length) {
    const numberOfHolidays = Number(holiday.length);
    return numberOfHolidays;
  }
};

const countNumberOfWorkingDaysOfTheWeek = (start_date, end_date) => {
  const startDate = new Date(start_date);
  const endDate = new Date(end_date);

  if (startDate > endDate)
    throw new BadRequest(
      'Por favor informe corretamente a data de início e fim da prestação de serviço',
    );

  const millisecondsPerDay = 86400 * 1000;
  startDate.setHours(0, 0, 0, 1);
  endDate.setHours(23, 59, 59, 999);
  const diff = endDate - startDate;
  let days = Math.ceil(diff / millisecondsPerDay);

  const weeks = Math.floor(days / 7);
  days = days - weeks * 2;

  const startDay = startDate.getDay();
  const endDay = endDate.getDay();

  if (startDay - endDay > 1) days = days - 2;
  if (startDay == 0 && endDay != 6) days = days - 1;
  if (endDay == 6 && startDay != 0) days = days - 1;

  const numberOfWorkingDaysOfTheWeek = Number(days);
  return numberOfWorkingDaysOfTheWeek;
};
