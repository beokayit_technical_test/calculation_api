import { Router } from 'express';
import { body, validationResult } from 'express-validator';

import calculationControllers from '../controllers/calculation-controllers';

const routes = Router();

routes.post('/calculo',
  [
    body('cnpj')
      .isNumeric()
      .isLength({ min: 14, max: 14 })
      .withMessage('Por favor digite os 14 dígitos numéricos do CNPJ'),
    body('data_inicio')
      .isDate()
      .withMessage('Por favor difite a data de início'),
    body('data_fim')
      .isDate()
      .withMessage('Por favor difite a data de fim'),
  ],
  (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ message: errors.array() });
    }
    next();
  },
);

routes.post('/calculo', calculationControllers.amountToPay);

export default routes;
