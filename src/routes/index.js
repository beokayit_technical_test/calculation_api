import { Router } from 'express';

import calculation from './calculation-routes';

const router = Router();

router.use(calculation);

export default router;
