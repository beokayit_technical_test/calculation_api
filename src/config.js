import dotenv from 'dotenv';
import path from 'path';

dotenv.config({
  path: path.resolve(__dirname, '../.env'),
});

export const PORT = process.env.PORT || 3000;
export const BASE_URL = process.env.BASE_URL_AXIOS || 'localhost:3000';
